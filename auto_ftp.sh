#!/bin/bash
#用途：自动化ftp传输

HOST='domain.com'                  #正式使用替换成对应的IP地址
USER='foo'                         #使用时，替换成相应的用户名
PASSWORD='password'                #替换成相应的用户密码
TRANSMODE='binary'                 #传输方式
TRANFILEPATH='/home/foo/test/'     #文件传输路径,使用绝对路径
PUTFILENAME='test.txt'
GETFILENAME='get.txt'

### 进行ftp连接
ftp -i -n $HOST << EOF
user ${USER} ${PASSWORD}'
${TRANSMODE}
cd ${TRANFILEPATH}
put ${PUTFILENAME}
get ${GETFILENAME}
quit
EOF


