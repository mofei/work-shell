#!/bin/bash
# 用途：用于向指定用户纪录的终端发送信息

USER=$1
DEVICES=`ls /dev/pts/* -l | awk '{print $3, $NF}' | grep $USER | awk '{print $2}'` #tty可能不是$9,换成$NF更好

for DEV in $DEVICES;
do
	cat /dev/stdin > $DEV
done
