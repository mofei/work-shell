#!/bin/bash
# 用途：获取最常使用的10条命令

printf "命令行\t使用次数\n" ;

awk '{list[$1]++;} \
END {
for(i in list) {
	printf("%s\t%d\n", i, list[i]); 
}
}' ~/.bash_history| sort -nrk 2 | head