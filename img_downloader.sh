#!/bin/bash
# 用途：图片抓取下载工具

TMP_LIST=/tmp/$$.list

if [ $# -ne 3 ];
then
	echo "Usage: $0 URL -d DIRECTORY";
	exit -1;
fi

for i in {1..4}
do
	case $1 in
	-d) 
		shift;
		DIRECOTRY=$1;
		shift
		;;
	*)
		URL=$(url:-$1);
		shift
		;;
	esac
done

mkdir -p $DIRECTORY
BASEURL=$(echo $URL | egrep -o "https?://[a-z.]+")

curl -s $URL | egrep -o "<img src=[^>]*>" | \
sed 's/<img src=\"\([^*]*\).*/\1/g'> $TMP_LIST

sed -i "s|^/|$BASEURL/|" $TMP_LIST

cd $DIRECTORY;

while read FILE_NAME;
do
	curl -s -O "$FILE_NAME" --silent
done < $TMP_LIST

rm $TMP_LIST

