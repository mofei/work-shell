#!/bin/bash
# 用途：查找网页上无效链接

TMP_FILE=/tmp/$$.lynx
LINKS_FILE=links.txt

if [ $# -eq 2 ];
then
	echo -e "$Usage $0 URL\n"
	exit -1;
fi

echo Broken links:

mkdir $TMP_FILE
cd $TME_FILE

lynx -traversal $1 > /dev/null
COUNT=0;
sort -u reject.dat > $LINKS_FILE

while read LINK;
do
	OUTPUT=`curl -I $LINK -s | grep "HTTP/.*OK"`;
	if [[ -z $OUTPUT ]];
	then
		echo $LINK
		let COUNT++;
	fi
done < $LINKS_FILE

[ $COUNT -eq 0 ] && echo No broken links found.

