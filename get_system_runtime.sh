#!/bin/bash
#利用ssh在远程主机上执行uptime命令来监控主机运行的时间
#要实现自动化监控，需要先配置ssh，可以免去ssh登陆主机输入密码的麻烦

IP_ADDRESS_LIST="192.168.0.1"            #要监控的主机列表
USER_NAME="test"                         #登入远程主机的用户名

for IP in $IP_ADDRESS_LIST;
do
	RUN_TIME=$(ssh -C $USER_NAME@$IP uptime | awk '{ print $3 }')
	echo $IP run_time: $RUN_TIME > system_runtime.txt
done

